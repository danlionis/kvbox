# kvbox

kvbox provides a simple key/value storage for nodejs. It works by using independent storage adapters.
This project was created because [keyv](https://github.com/lukechilds/keyv) sadly doesn't implement the whole Map API.

## Features

-   Promise based API
-   Namespaces

## Usage

Install kvbox

```
npm install kvbox
```

Create an instance and storage adapter. If no storage adapter is provided default to JavaScripts [`Map`](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Map)

```ts
import Box from "kvbox";

const kvbox = new Box(); // store: new Map()

await kvbox.set("foo", "bar");
await kvbox.get("foo"); // "bar"
await kvbox.delete("foo"); // true
await kvbox.clear(); // void
```

## Store

Out of the box kvbox includes a file system adapter, that stores the values in a simple json file.
Alternatively you can define your own adapters.

## File System Adapter

```ts
import { Box, FileStore } from "kvbox";

const store = new FileStore({ path: "data.json" });
const box = new Box({ store: store });
```

### Custom Adapter

You can use any Adatper that implements the [`Map`](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Map) Api (without the forEach method).

To write custom adapters you have to extend your adapter with the `StorageAdapter` class and implement all required methods.

```ts
import { StorageAdapter } from "kvbox";

class ExampleAdapter extends StorageAdapter {
    /* implementation omitted */
}
```

## Namespaces

To prevent key collisions when using the same storage adapter, you can declare a namespace

```ts
import { Box, FileStore } from "kvbox";

const store = new FileStore();
const one = new Box({ store: store, namespace: "one" });
const two = new Box({ store: store, namespace: "two" });

await one.set("foo", "one");
await two.set("foo", "two");

await one.get("foo"); // "one"
await one.delete("foo"); // true
await two.get("foo"); // "two"
```

## Serializer

By default kvbox uses the [flatted](https://npmjs.com/package/flatted) JSON parser. You can use your own serializer by declaring it when constructin your Box. This example uses nodejs standard `JSON.stringify` and `JSON.parse` methods

```ts
import { Box } from "kvbox";

const box = new Box({
    serialize: JSON.stringify,
    deserialize: JSON.parse,
});
```
