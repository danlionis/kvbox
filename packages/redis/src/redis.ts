// @ts-ignore
import { StorageAdapter } from "kvbox";
import * as redis from "redis";

export class RedisStore extends StorageAdapter {
    public static async init(opts?: redis.ClientOpts) {
        return new Promise<RedisStore>((resolve, reject) => {
            const client = redis.createClient(opts);
            client.once("connect", () => {
                resolve(new RedisStore(client));
            });
            client.once("error", (err) => {
                reject(err);
                client.quit();
            });
        });
    }

    private readonly client: redis.RedisClient;

    constructor(client: redis.RedisClient) {
        super();
        this.client = client;
    }

    public get(key: string) {
        return new Promise<string>((resolve) => {
            this.client.get(key, (err, res) => {
                if (err) {
                    resolve(undefined);
                } else {
                    resolve(res);
                }
            });
        });
    }

    public set(key: string, value: string, ttl: number) {
        return new Promise<void>((resolve) => {
            if (ttl === undefined) {
                this.client.set(key, value, () => {
                    resolve();
                });
            } else {
                this.client.set(key, value, "PX", ttl, () => {
                    resolve();
                });
            }
        });
    }

    public delete(key: string) {
        return new Promise<boolean>((resolve) => {
            this.client.del(key, (_, res) => {
                if (res > 0) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    }

    public keys(namespace: string) {
        return new Promise<string[]>((resolve) => {
            this.client.keys(`${namespace}:*`, (err, res) => {
                resolve(res);
            });
        });
    }

    public clear(namespace: string) {
        return new Promise<void>((resolve) => {
            this.client.keys(`${namespace}:*`, async (err, keys) => {
                await Promise.all(keys.map((k) => this.delete(k)));
                resolve();
            });
        });
    }

    public has(key: string) {
        return new Promise<boolean>((resolve) => {
            this.client.exists(key, (err, res) => {
                if (res === 1) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    }

    public async values(namespace: string) {
        const keys = await this.keys(namespace);
        const values = await Promise.all(
            keys.map(async (k) => {
                const v = await this.get(k);
                return v;
            }),
        );

        return values;
    }

    public async entries(namespace: string) {
        const keys = await this.keys(namespace);
        const entries: Array<[string, string]> = await Promise.all(
            keys.map<Promise<[string, string]>>(async (k) => {
                const v = await this.get(k);
                return [k, v];
            }),
        );
        return entries;
    }

    public close() {
        return new Promise<void>((resolve) => {
            this.client.quit(() => {
                resolve();
            });
        });
    }
}

export default RedisStore;
