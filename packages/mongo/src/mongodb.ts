// @ts-ignore
import { StorageAdapter } from "kvbox";
import * as mongo from "mongodb";

interface MongoStoreOpts {
    db?: string;
    url?: string;
    collection?: string;
}

export class MongoStore extends StorageAdapter {
    public static async init(
        url: string = "mongodb://localhost:27017",
        opts?: mongo.MongoClientOptions,
        db: string = "kvbox",
        collection: string = "kvbox",
    ): Promise<StorageAdapter> {
        try {
            const client = await mongo.MongoClient.connect(url, {
                useNewUrlParser: true,
                ...opts,
            });
            return new MongoStore(client, db, collection);
        } catch (err) {
            throw err;
        }
    }

    // private readonly opts: MongoStoreOpts & mongo.MongoClientOptions;
    private client: mongo.MongoClient;
    private collection: mongo.Collection<{ key: string; value: string }>;

    private constructor(
        client: mongo.MongoClient,
        db: string,
        collection: string,
    ) {
        super();

        this.client = client;
        this.collection = this.client.db(db).collection(collection);
    }

    public async set(key: string, value: string) {
        const doc = { key, value };

        await this.collection.updateOne(
            { key: key },
            { $set: doc },
            { upsert: true },
        );
    }

    public async get(key: string) {
        const res = await this.collection.findOne<{
            key: string;
            value: string;
        }>({
            key: key,
        });
        if (res === null) {
            return undefined;
        }
        return res.value;
    }

    public async delete(key: string) {
        try {
            const res = await this.collection.deleteOne({ key: key });
            if (res.result.n > 0) {
                return true;
            }
            return false;
        } catch (e) {
            return false;
        }
    }

    public async has(key: string) {
        try {
            const res = await this.collection.findOne(
                { key: key },
                { projection: { _id: 1 } },
            );
            if (res) {
                return true;
            }
            return false;
        } catch {
            return false;
        }
    }

    public async values(namespace: string) {
        return new Promise<string[]>((resolve) => {
            this.collection
                .find(
                    { key: new RegExp(`${namespace}:.*`) },
                    { projection: { value: true } },
                )
                .toArray((err, docs) => {
                    const res: string[] = docs.map((d) => {
                        return d.value;
                    });
                    resolve(res);
                });
        });
    }

    public async keys(namespace: string) {
        return new Promise<string[]>((resolve) => {
            this.collection
                .find(
                    { key: new RegExp(`${namespace}:.*`) },
                    { projection: { key: true } },
                )
                .toArray((err, docs) => {
                    const res: string[] = docs.map((d) => {
                        return d.key;
                    });
                    resolve(res);
                });
        });
    }

    public async entries(namespace: string) {
        return new Promise<Array<[string, string]>>((resolve) => {
            this.collection
                .find({ key: new RegExp(`${namespace}:.*`) })
                .toArray((err, docs) => {
                    if (err) {
                        resolve([]);
                    }
                    const res: Array<[string, string]> = docs.map((d) => {
                        return [d.key, d.value];
                    });
                    resolve(res);
                });
        });
    }

    public async clear(namespace: string) {
        await this.collection.deleteMany({
            key: new RegExp(`${namespace}:.*`),
        });
        return;
    }

    public async close() {
        return this.client.close();
    }

    public async dump() {
        const docs = await this.collection
            .find({}, { projection: { key: true, value: true, _id: false } })
            .toArray();

        console.log(docs);
    }
}

export default MongoStore;
