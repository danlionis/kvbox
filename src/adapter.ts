import { EventEmitter } from "events";

export abstract class StorageAdapter extends EventEmitter
    implements IStorageAdapter {
    public readonly nativeTtl: boolean = false;
    /**
     * Count how many instances of Datastore use this StorageAdapter
     * the close() method is only called if this count is 0
     */
    public instanceCount = 0;

    public abstract get(key: string): string | Promise<string>;

    public abstract set(
        key: string,
        value: string,
        ttl?: number,
    ): void | Promise<void>;

    public abstract delete(key: string): boolean | Promise<boolean>;

    public abstract clear(namespace: string): void | Promise<void>;

    public has(key: string): boolean | Promise<boolean> {
        return new Promise(async (resolve) => {
            const get = await this.get(key);
            resolve(get !== undefined);
        });
    }

    public abstract entries(
        namespace: string,
    ): Iterable<[string, string]> | Promise<Iterable<[string, string]>>;

    public keys(
        namespace: string,
    ): Iterable<string> | Promise<Iterable<string>> {
        return new Promise(async (resolve) => {
            const entries = await this.entries(namespace);
            const keys: string[] = [];
            for (const entry of entries) {
                keys.push(entry[0]);
            }
            resolve(keys);
        });
    }

    public values(
        namespace: string,
    ): Iterable<string> | Promise<Iterable<string>> {
        return new Promise(async (resolve) => {
            const entries = await this.entries(namespace);
            const values: string[] = [];
            for (const entry of entries) {
                values.push(entry[1]);
            }
            resolve(values);
        });
    }

    public close(): void | Promise<void> {}
}

export interface IStorageAdapter {
    get(key: string): string | Promise<string>;
    set(key: string, value: string, ttl?: number): void;
    delete(key: string): boolean | Promise<boolean>;
    clear(namespace: string): void | Promise<void>;
    has(key: string): boolean | Promise<boolean>;
    keys(namespace: string): Iterable<string> | Promise<Iterable<string>>;
    entries(
        namespace: string,
    ): Iterable<[string, string]> | Promise<Iterable<[string, string]>>;
    values(namespace: string): Iterable<string> | Promise<Iterable<string>>;
}
