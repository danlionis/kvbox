import * as flatted from "flatted";
import { IStorageAdapter, StorageAdapter } from "./adapter";
import { EventEmitter } from "events";

interface BoxOpts {
    store: IStorageAdapter;
    namespace: string;
    serialize: (obj: any) => string;
    deserialize: (obj: string) => any;
    /**
     * Interval in which all entries are checked if not expired and deleted if expired
     *
     * Has no effect if the store supports nativeTTL
     *
     * Set to 0 to disable
     *
     * default: 0
     */
    clearExpiredInterval: number;
}

interface Entry {
    value: unknown;
    expires: number;
}

function expired(expires: number): boolean {
    if (expires === null) {
        return false;
    }
    return expires < Date.now();
}

export class Box extends EventEmitter {
    private opts: BoxOpts;

    private readonly storeIsAdapter: boolean;
    private readonly storeNativeTtl: boolean;

    private interval: NodeJS.Timeout;

    constructor(opts?: Partial<BoxOpts>) {
        super();
        const defaultOpts: BoxOpts = {
            namespace: "default",
            serialize: flatted.stringify,
            deserialize: flatted.parse,
            store: new Map<string, string>(),
            clearExpiredInterval: 0,
        };
        this.opts = Object.assign(defaultOpts, opts);

        if (this.opts.store instanceof StorageAdapter) {
            this.opts.store.instanceCount += 1;
        }

        this.storeIsAdapter = this.opts.store instanceof StorageAdapter;
        this.storeNativeTtl =
            this.opts.store instanceof StorageAdapter &&
            this.opts.store.nativeTtl;

        if (!this.storeNativeTtl && this.opts.clearExpiredInterval > 0) {
            this.addListener("clearExpired", this.clearExpired);
            this.interval = setInterval(() => {
                this.emit("clearExpired");
            }, this.opts.clearExpiredInterval);
        }
    }

    /**
     * Delete the value saved under a key
     * @param key key of the entry you want to delete
     */
    public async delete(key: string) {
        key = this.getPrefix(key);
        return this.opts.store.delete(key);
    }

    /**
     * Get the value of an entry associated with the given key
     * @param key key of the entry
     */
    public async get<T>(key: string): Promise<T> {
        key = this.getPrefix(key);
        const value = await this.opts.store.get(key);
        if (value === undefined) {
            return undefined;
        }
        const res: Entry = this.opts.deserialize(value);
        if (expired(res.expires)) {
            this.opts.store.delete(key);
            return undefined;
        }
        return res.value as T;
    }

    /**
     * Save a value in the datastore
     *
     * @param key key under which the value should be stored
     * @param value value that should be stored
     * @param ttl time to live in ms
     */
    public async set(key: string, value: any, ttl?: number): Promise<void> {
        key = this.getPrefix(key);
        const expires = ttl !== undefined ? Date.now() + ttl : null;
        const stringValue = this.opts.serialize({ value, expires });
        return this.opts.store.set(key, stringValue, expires);
    }

    /**
     * Return true if there is an entry with the given key
     * @param key
     */
    public async has(key: string): Promise<boolean> {
        const prefixedKey = this.getPrefix(key);

        if (this.storeNativeTtl) {
            return this.opts.store.has(prefixedKey);
        }

        return (await this.get(key)) !== undefined;
    }

    /**
     * Clear all values saved under the current namespace
     */
    public async clear() {
        if (this.storeIsAdapter) {
            return this.opts.store.clear(this.opts.namespace);
        }

        const keys = await this.keys();

        await Promise.all(keys.map((key) => this.delete(key)));
        return;
    }

    /**
     * Returns an array of keys saved under the current namespace
     */
    public async keys(): Promise<string[]> {
        if (this.storeNativeTtl) {
            const keys = await this.opts.store.keys(this.opts.namespace);
            let arr = Array.from(keys);
            arr = arr.map((k) => this.removeNamespace(k));
            return arr;
        } else {
            // entries() handles ttl validation
            const entries = await this.entries();
            return entries.map(([k, _]) => k);
        }
    }

    /**
     * Returns an array of key/value pairs of all entries under the current namespace
     */
    public async entries(): Promise<[string, any][]> {
        const entries = await this.opts.store.entries(this.opts.namespace);
        const arr = Array.from(entries);
        const res: [string, any][] = arr

            // deserialize all raw values
            .map(([key, value]) => [key, this.deserializeRawValue(value)])

            // filter all expired
            .filter(([key, value]: [string, Entry]) => {
                const notExpired = !expired(value.expires);
                if (this.storeIsAdapter) {
                    return notExpired;
                }

                // namespaces are handled by the StorageAdapter
                return key.startsWith(this.opts.namespace + ":") && notExpired;
            })

            // map to actual value
            .map((entry: [string, Entry]) => {
                const k = this.removeNamespace(entry[0]);
                const v = entry[1].value;
                return [k, v];
            });
        return res;
    }

    /**
     * Returns an array of values saved under the current namespace
     */
    public async values(): Promise<any[]> {
        if (this.storeIsAdapter) {
            const values = await this.opts.store.values(this.opts.namespace);
            const arr = Array.from(values);

            const res: any[] = arr
                .map((v) => this.deserializeRawValue(v))
                .filter((d) => !expired(d.expires))
                .map((d) => d.value);
            return res;
        } else {
            const entries = await this.entries();
            return entries.map(([_, value]) => value);
        }
    }

    /**
     * Attempt to close the connection to the storage Adapter
     *
     * This function will decrease the instanceCount of the StorageAdapter
     * If the instanceCount equals 0 the StorageAdapter will be closed
     */
    public async close() {
        if (this.opts.store instanceof StorageAdapter) {
            this.opts.store.instanceCount -= 1;
            if (this.opts.store.instanceCount === 0) {
                return this.opts.store.close();
            }
        }
    }

    /**
     * Clears all entries under this namespace that are expired
     */
    public async clearExpired() {
        const entries = await this.opts.store.entries(this.opts.namespace);
        Array.from(entries)
            // deserialize all raw values
            .map(([key, value]) => [key, this.deserializeRawValue(value)])
            .filter(([_, value]: [string, Entry]) => expired(value.expires))
            .forEach(([key, _]: [string, Entry]) => {
                this.opts.store.delete(key);
            });
    }

    /**
     * Returns the key with the prefixed namespace
     *
     * ```
     * let box = new Box({ namespace: "test" });
     * let key = "one";
     * box.getPrefix(key); // test:one
     * ```
     */
    private getPrefix(key: string) {
        return `${this.opts.namespace}:${key}`;
    }

    /**
     * Get the key without the leading namespace
     * @param key
     *
     * ```
     * let box = new Box({ namespace: "test" });
     * let key = "test:one";
     * box.removeNamespace(key); // one
     * ```
     */
    private removeNamespace(key: string) {
        return key.substr(this.opts.namespace.length + 1);
    }

    private deserializeRawValue(rawValue: string): Entry {
        return this.opts.deserialize(rawValue) as Entry;
    }
}

export default Box;
