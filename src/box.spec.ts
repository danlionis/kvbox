import { Box, StorageAdapter } from "./";

class MockStore extends StorageAdapter {
    public static async init() {
        return new MockStore();
    }

    public readonly nativeTtl = true;
    private store = new Map<string, string>();
    private constructor() {
        super();
    }
    public get(key: string): string | Promise<string> {
        return this.store.get(key);
    }
    public set(key: string, value: string): void {
        this.store.set(key, value);
    }
    public delete(key: string): boolean | Promise<boolean> {
        return this.store.delete(key);
    }
    public clear(namespace: string): void | Promise<void> {
        this.store.clear();
    }
    public entries(
        namespace: string,
    ): Iterable<[string, string]> | Promise<Iterable<[string, string]>> {
        return Array.from(this.store.entries()).filter(([key, _]) =>
            key.startsWith(namespace + ":"),
        );
    }
}

const timeout = (ms: number) => {
    return new Promise<void>((resolve) => {
        setTimeout(() => {
            resolve();
        }, ms);
    });
};

let box: Box;
let store: StorageAdapter;

beforeAll(async () => {
    store = await MockStore.init();
    box = new Box({
        namespace: "test",
    });
});

afterAll(async () => {
    await box.close();
});

beforeEach(async () => {
    await box.clear();
});

describe("init", () => {
    test("no storage adapter", async () => {
        const b1 = new Box();

        await b1.set("key", "value");
        expect(await b1.get("key")).toBe("value");

        const keys = await b1.keys();
        const values = await b1.values();
        const entries = await b1.entries();

        expect(keys).toEqual(expect.arrayContaining(["key"]));
        expect(values).toEqual(expect.arrayContaining(["value"]));
        expect(entries).toEqual(expect.arrayContaining([["key", "value"]]));

        await b1.clear();
        await b1.close();
    });

    test("native ttl", async () => {
        const b1 = new Box({
            store: await MockStore.init(),
        });
        await b1.set("key", "value");
        expect(await b1.get("key")).toBe("value");

        const keys = await b1.keys();
        const values = await b1.values();
        const entries = await b1.entries();

        expect(keys).toEqual(expect.arrayContaining(["key"]));
        expect(values).toEqual(expect.arrayContaining(["value"]));
        expect(entries).toEqual(expect.arrayContaining([["key", "value"]]));

        await b1.clear();
        await b1.close();
    });

    test("identical storage (no StorageAdapter)", async () => {
        const mapStore = new Map();
        const b1 = new Box({ store: mapStore, namespace: "b1" });
        const b2 = new Box({ store: mapStore, namespace: "b2" });

        await b1.set("key", 1);
        await b2.set("key", 2);

        expect(await b1.get("key")).toBe(1);
        expect(await b2.get("key")).toBe(2);

        expect(await b1.entries()).toEqual(
            expect.arrayContaining([["key", 1]]),
        );
        expect(await b2.entries()).toEqual(
            expect.arrayContaining([["key", 2]]),
        );

        await b1.clear();
        expect(await b1.entries()).toHaveLength(0);
        expect(await b2.entries()).toHaveLength(1);
        await b2.clear();
        expect(await b2.entries()).toHaveLength(0);
    });
});

describe("insert", () => {
    test("string", async () => {
        await box.set("key", "string");
        expect(await box.get("key")).toBe("string");
    });

    test("number", async () => {
        await box.set("key", 0);
        expect(await box.get("key")).toBe(0);
    });

    test("boolean", async () => {
        await box.set("true", true);
        await box.set("false", false);
        expect(await box.get("true")).toBe(true);
        expect(await box.get("false")).toBe(false);
    });

    test("object", async () => {
        const obj = {
            str: "string",
            num: 10,
            bool: false,
            arr: [0, "1", false],
            self: null,
        };
        obj.self = obj;
        await box.set("key", obj);
        expect(await box.get("key")).toStrictEqual(obj);
    });

    test("null", async () => {
        await box.set("key", null);
        expect(await box.get("key")).toBeNull();
    });

    test("undefined", async () => {
        await box.set("key", undefined);
        expect(await box.get("key")).toBeUndefined();
    });
});

describe("get", () => {
    test("existing", async () => {
        await box.set("key", "value");
        expect(await box.get("key")).toBe("value");
    });

    test("missing", async () => {
        expect(await box.get("key")).toBeUndefined();
    });
});

describe("has", () => {
    test("existing", async () => {
        await box.set("key", "value");
        expect(await box.has("key")).toBe(true);
    });

    test("missing", async () => {
        expect(await box.has("key")).toBe(false);
    });
});

describe("delete", () => {
    test("existing", async () => {
        await box.set("key", "value");
        expect(await box.delete("key")).toBe(true);
    });

    test("missing", async () => {
        expect(await box.delete("key")).toBe(false);
    });

    test("clear", async () => {
        // const seconds = new Box({
        //     store: fileStore,
        //     namespace:
        // });

        await box.set("key", "value");
        expect(await box.keys()).toHaveLength(1);
        await box.clear();
        expect(await box.keys()).toHaveLength(0);
    });
});

describe("clear", () => {
    test("single namespace", async () => {
        await box.set("key", "value");
        expect(await box.keys()).toHaveLength(1);
        await box.clear();
        expect(await box.keys()).toHaveLength(0);
    });

    test("multiple namespaces", async () => {
        const second = new Box({
            store: store,
            namespace: "second",
        });

        await box.set("key", "value");
        await second.set("key2", "value2");
        expect(await box.keys()).toHaveLength(1);
        expect(await second.keys()).toHaveLength(1);
        await box.clear();
        expect(await box.keys()).toHaveLength(0);
        expect(await second.keys()).toHaveLength(1);
        await second.clear();
        expect(await second.keys()).toHaveLength(0);
        await second.close();
    });
});

describe("mockadapter", () => {
    test("close single", async () => {
        const mockStore = await MockStore.init();
        const db = new Box({ store: mockStore });
        expect(mockStore.instanceCount).toBe(1);
        await db.close();
        expect(mockStore.instanceCount).toBe(0);
    });

    test("close multiple", async () => {
        const mockStore = await MockStore.init();
        const db1 = new Box({ store: mockStore });

        expect(mockStore.instanceCount).toBe(1);

        const db2 = new Box({ store: mockStore });

        expect(mockStore.instanceCount).toBe(2);

        await db1.close();
        expect(mockStore.instanceCount).toBe(1);

        await db2.close();
        expect(mockStore.instanceCount).toBe(0);
    });
});

describe("adapter defaults", () => {
    test("has", async () => {
        const mockStore = await MockStore.init();
        const db = new Box({ store: mockStore });
        await db.set("has", 1);
        expect(await db.has("has")).toBe(true);
    });
});

describe("ttl", () => {
    test("no ttl", async () => {
        await box.set("ttl", 0);
        expect(await box.has("ttl")).toBe(true);
    });

    test("get", async () => {
        await box.set("ttl", 0, 100);
        expect(await box.get("ttl")).toBe(0);
        await timeout(200);
        expect(await box.get("ttl")).toBeUndefined();
    });

    test("has", async () => {
        expect(await box.has("ttl")).toBe(false);
        await box.set("ttl", 0, 100);
        expect(await box.has("ttl")).toBe(true);
        await timeout(200);
        expect(await box.has("ttl")).toBe(false);
    });
});

describe("misc", () => {
    test("keys", async () => {
        await box.set("k1", "a");
        await box.set("k2", "b");
        await box.set("k3", "c");

        expect(await box.keys()).toEqual(
            expect.arrayContaining(["k1", "k2", "k3"]),
        );
    });

    test("values", async () => {
        await box.set("v1", "a");
        await box.set("v2", "b");
        await box.set("v3", "c");

        expect(await box.values()).toEqual(
            expect.arrayContaining(["a", "b", "c"]),
        );
    });

    test("entries", async () => {
        await box.set("e1", "a");
        await box.set("e2", "b");
        await box.set("e3", "c");

        const expected = [
            ["e1", "a"],
            ["e2", "b"],
            ["e3", "c"],
        ];

        expect(await box.entries()).toEqual(expect.arrayContaining(expected));
    });
});
