import * as fs from "fs";
import * as path from "path";
import * as rimraf from "rimraf";
import Box from "..";
import { FileStore } from "./file";

// let store: FileStore;

const filePath = ".cache/test.json";
const corruptedDir = ".cache/corrupted";
let box: Box;

let store: FileStore;

beforeAll(async () => {
    store = await FileStore.init({
        path: filePath,
    });
    box = new Box({
        store,
    });
});

afterAll(async () => {
    await box.clear();
    await box.close();
});

beforeEach(async () => {
    await box.clear();
});

describe("misc", () => {
    test("keys", async () => {
        const b1 = new Box({ store, namespace: "keys" });

        await b1.set("one", "1");
        await b1.set("two", "2");
        await b1.set("three", "3");

        expect(await store.keys("keys")).toEqual(
            expect.arrayContaining(["keys:one", "keys:two", "keys:three"]),
        );

        await b1.clear();
        b1.close();
    });

    test("has", async () => {
        await box.set("has", 1);
        expect(await box.has("has")).toBe(true);
    });

    test("delete", async () => {
        await box.set("delete", true);
        expect(await box.has("delete")).toBe(true);
        expect(await box.delete("delete")).toBe(true);
        expect(await box.has("delete")).toBe(false);
    });

    test("multile box instances", async () => {
        await box.set("clear", 1);
        const box2 = new Box({ store: store, namespace: "box2" });
        await box2.clear();
        expect(await box.entries()).toHaveLength(1);
        await box2.close();
    });
});

describe("init", () => {
    test("no file", async () => {
        try {
            fs.unlinkSync(filePath);
        } catch {}
        expect(() => fs.readFileSync(filePath)).toThrowError();
        const _ = new FileStore({
            path: filePath,
        });

        expect(fs.readFileSync(filePath).toString()).toBe("[]");
    });

    test("corrupted file", () => {
        rimraf.sync(corruptedDir);
        fs.writeFileSync(filePath, "corrupted file");

        const _ = new FileStore({
            path: filePath,
            saveCorrupted: true,
        });
        const str = (num: number) => {
            if (num < 10) {
                return "0" + num;
            }
            return num.toString();
        };

        const date = new Date();
        const dateStr =
            date.getFullYear() +
            str(date.getMonth()) +
            str(date.getDate()) +
            str(date.getHours()) +
            str(date.getMinutes()) +
            str(date.getSeconds());

        const corruptedFiles = fs.readdirSync(corruptedDir);

        const expectedCorruptedFile = `.${dateStr}.${path.basename(filePath)}`;

        expect(corruptedFiles).toEqual(
            expect.arrayContaining([expectedCorruptedFile]),
        );
        expect(fs.readFileSync(filePath).toString()).toBe("[]");
    });

    test("data available", async () => {
        // write data
        let b: Box;
        b = new Box({ store });
        await b.set("available", 1);
        b.close();

        const readStore = await FileStore.init({
            path: filePath,
        });
        b = new Box({ store: readStore });
        expect(await b.get("available")).toBe(1);
        await b.close();
    });
});
