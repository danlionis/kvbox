import * as fs from "fs";
import * as path from "path";
import { StorageAdapter } from "../adapter";

interface FileStoreOpts {
    /**
     * Location of the savefile
     */
    path: string;
    /**
     * Whether or not to backup a file in case it is corrupted and cannot be manipulated
     */
    saveCorrupted: boolean;
}

export class FileStore extends StorageAdapter {
    public static async init(opts?: Partial<FileStoreOpts>) {
        return new FileStore(opts);
    }

    private opts: FileStoreOpts;

    private cache: Map<string, string> = new Map();

    constructor(opts?: Partial<FileStoreOpts>) {
        super();
        const defaultOpts: FileStoreOpts = {
            path: "./.cache/kvbox.json",
            saveCorrupted: false,
        };

        this.opts = Object.assign(defaultOpts, opts);

        this.opts.path = path.normalize(this.opts.path);

        fs.mkdirSync(path.dirname(this.opts.path), { recursive: true });

        let fileContent: string;

        try {
            fileContent = fs.readFileSync(this.opts.path).toString();
        } catch {
            this.saveFile();
        }

        try {
            this.cache = this.jsonToMap(JSON.parse(fileContent));
        } catch (err) {
            // file is corrupted
            if (this.opts.saveCorrupted) {
                this.renameCorruptedFile();
            }
            this.saveFile();
        }
    }

    public async get(key: string): Promise<string> {
        return this.cache.get(key);
        // return this.cache[key];
    }

    public async set(key: string, value: string): Promise<void> {
        this.cache.set(key, value);
        this.saveFile();
    }

    public async delete(key: string): Promise<boolean> {
        const deleted = this.cache.delete(key);
        this.saveFile();
        return deleted;
    }

    public async clear(namespace: string) {
        for (const key of this.cache.keys()) {
            if (key.startsWith(`${namespace}:`)) {
                this.cache.delete(key);
            }
        }
        this.saveFile();
    }

    public async entries(namespace: string): Promise<[string, string][]> {
        return Array.from(this.cache.entries()).filter(([k, _]) =>
            k.startsWith(`${namespace}:`),
        );
    }

    private renameCorruptedFile() {
        const dirname = path.dirname(this.opts.path);
        const basename = path.basename(this.opts.path);

        fs.mkdirSync(path.join(dirname, "corrupted"), { recursive: true });

        const str = (num: number) => {
            if (num < 10) {
                return "0" + num;
            }
            return num.toString();
        };

        const date = new Date();
        const dateStr =
            date.getFullYear() +
            str(date.getMonth()) +
            str(date.getDate()) +
            str(date.getHours()) +
            str(date.getMinutes()) +
            str(date.getSeconds());

        fs.renameSync(
            path.normalize(this.opts.path),
            path.normalize(
                // path.join(dirname, `.${new Date().toISOString()}.${basename}`),
                path.join(dirname, "corrupted", `.${dateStr}.${basename}`),
            ),
        );
    }

    private saveFile() {
        fs.writeFileSync(
            this.opts.path,
            JSON.stringify(Array.from(this.cache.entries())),
        );
    }

    private jsonToMap(data: [string, string][]) {
        // private jsonToMap(data: Array<[string, string]>) {
        const map = new Map<string, string>();
        for (const entry of data) {
            map.set(entry[0], entry[1]);
        }
        return map;
    }
}
